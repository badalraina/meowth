package com.meowth;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;


import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;

public class MeowthJob extends Job {

    public static final String TAG = "job_meowth";

    private Context context;
    private String token;

    @Override
    @NonNull
    protected Result onRunJob(Params params) {
        Meowth m = new Meowth();
        m.registerNetworkReciever(getContext());
        m.submitData(context, token);
        return Result.SUCCESS;
    }

    static void scheduleJob() {
        Bundle transientExtras = new Bundle();
        JobRequest.Builder jobRequestBuilder = new JobRequest.Builder(MeowthJob.TAG)
                .setRequiresBatteryNotLow(true)
                .setUpdateCurrent(true)
                .setPeriodic(30*60*1000, 5*60*1000)
                .setTransientExtras(transientExtras);

        jobRequestBuilder.build().schedule();
    }
}